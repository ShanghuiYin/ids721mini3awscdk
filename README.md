# IDS721 Spring 2024 Weekly Mini Project 3

Create an S3 Bucket using CDK with AWS CodeWhisperer. 

## Requirements
1. Create S3 bucket using AWS CDK
2. Use CodeWhisperer to generate CDK code
3. Add bucket properties like versioning and encryption

## Setup
1. Initiate a New CDK Project `cdk init app --language=typescript`
2. Invoke CodeWhisperer: With the project set up, you would typically write a comment in your CDK stack file describing what you want to do. For instance: 
```python
// Prompt
// create an S3 bucket with versioning enabled and AES-256 encryption
```

> Then you would see successfulness:

![alt text](image-3.png)


## Deploy

Deploy your CDK stack to AWS by running `cdk deploy`. This command provisions the S3 bucket with the specified properties in your AWS account.

> You would see:

![alt text](image-2.png)

## Bucket Settings

> Enable bucket versioning

![alt text](image-4.png)

> Encryption Info

![alt text](image-5.png)

## CodeWhisperer Integration

Start working with Amazon CodeWhisperer Individual in just a few minutes.

### Step 1: Install

Install the CodeWhisperer extension for your editor

Note: CodeWhisperer comes built in with AWS Cloud9 and the AWS Lambda console.

### Step 2: Authenticate

Sign in with Builder ID for Individual users and IAM Identity Center for Professional users.

### Step 3: Start building

Open your editor, and CodeWhisperer should just work! 

---
Once the CodeWhisperer is enabled. Auto fill will begin as follow:

![alt text](image.png)

If I press `tab`, I could generate the code I want.

![alt text](image-1.png)



## Reference
1. https://docs.aws.amazon.com/cdk/v2/guide/getting_started.html
2. https://docs.aws.amazon.com/cdk/v2/guide/hello_world.html
